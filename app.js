'use strict';

let input = parseInt(prompt('Get factorial of: '));

while (isNaN(+input) || input === '' || !input) {
    input = prompt('Enter a number: ', input);
}

const getFactorial = input => {
    return input <= 1 ? 1 : input * getFactorial(input - 1);
};

alert(getFactorial(input));